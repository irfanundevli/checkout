import produce from "immer";
import { Basket, BasketItem, Product } from "../components/App";
import { formatPrice } from "./price-formatter";

export function addProductToBasket(product: Product, basket: Basket): Basket {
  const index = basket.items.findIndex((item) => item.id === product.id);

  if (index >= 0) {
    const existingItem = basket.items[index];
    const newQuantity = existingItem.quantity + 1;

    return produce(basket, (draft) => {
      draft.items[index].quantity = newQuantity;
      draft.items[index].totalCost = calculateItemTotalCost(
        newQuantity,
        existingItem.unitCost
      );
      draft.totalCost = calculateBasketTotalCost(draft.items);
    });
  } else {
    return produce(basket, (draft) => {
      draft.items.push({
        id: product.id,
        name: product.name,
        unitCost: product.price,
        totalCost: formatCost(product.price),
        quantity: 1,
      });
      draft.totalCost = calculateBasketTotalCost(draft.items);
    });
  }
}

function calculateItemTotalCost(quantity: number, unitCost: number): string {
  return formatCost(quantity * unitCost);
}

function calculateBasketTotalCost(items: BasketItem[]): string {
  return formatCost(
    items.reduce((acc, item) => acc + item.quantity * item.unitCost, 0)
  );
}

function formatCost(cost: number): string {
  return formatPrice(cost);
}

export function resetAllBasketItemsQuantityToZero(basket: Basket): Basket {
  return produce(basket, (draft) => {
    draft.items = basket.items.map((item) => {
      return { ...item, totalCost: formatCost(0), quantity: 0 };
    });
    draft.totalCost = calculateBasketTotalCost(draft.items);
  });
}

export function removeItemFromBasketByItemId(
  basket: Basket,
  itemId: string
): Basket {
  return produce(basket, (draft) => {
    draft.items = basket.items.filter((item) => item.id !== itemId);
    draft.totalCost = calculateBasketTotalCost(draft.items);
  });
}

export function updateItemWithNewQuantity(
  basket: Basket,
  itemId: string,
  newQuantity: number
): Basket {
  const index = basket.items.findIndex((item) => item.id === itemId);
  const existingItem = basket.items[index];

  return produce(basket, (draft) => {
    draft.items[index].quantity = newQuantity;
    draft.items[index].totalCost = calculateItemTotalCost(
      newQuantity,
      existingItem.unitCost
    );
    draft.totalCost = calculateBasketTotalCost(draft.items);
  });
}
