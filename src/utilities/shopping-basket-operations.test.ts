import { Basket, Product } from "../components/App";
import {
  addProductToBasket,
  removeItemFromBasketByItemId,
  resetAllBasketItemsQuantityToZero,
  updateItemWithNewQuantity,
} from "./shopping-basket-operations";

describe("shopping basket operations", () => {
  describe("addProductToBasket", () => {
    it("should add product to basket", () => {
      const product: Product = {
        id: "1",
        name: "Product",
        price: 1000,
      };

      const emptyBasket: Basket = {
        totalCost: "0",
        items: [],
      };

      const result = addProductToBasket(product, emptyBasket);

      expect(result.totalCost).toEqual("$10.00");
      expect(result.items).toEqual([
        {
          id: product.id,
          name: product.name,
          quantity: 1,
          totalCost: "$10.00",
          unitCost: 1000,
        },
      ]);
    });
  });

  describe("resetAllBasketItemsQuantityToZero", () => {
    it("should reset basket items quantity to zero", () => {
      const basket: Basket = {
        totalCost: "$10.00",
        items: [
          {
            id: "1",
            name: "Product Name",
            quantity: 1,
            totalCost: "$10.00",
            unitCost: 1000,
          },
        ],
      };

      const result = resetAllBasketItemsQuantityToZero(basket);

      expect(result).toEqual({
        totalCost: "$0.00",
        items: [
          {
            id: "1",
            name: "Product Name",
            quantity: 0,
            totalCost: "$0.00",
            unitCost: 1000,
          },
        ],
      });
    });
  });

  describe("removeItemFromBasketByItemId", () => {
    it("should remove item from basket", () => {
      const basket: Basket = {
        totalCost: "$10.00",
        items: [
          {
            id: "1",
            name: "Product Name",
            quantity: 1,
            totalCost: "$10.00",
            unitCost: 1000,
          },
        ],
      };

      const result = removeItemFromBasketByItemId(basket, "1");

      expect(result).toEqual({
        totalCost: "$0.00",
        items: [],
      });
    });
  });

  describe("updateItemWithNewQuantity", () => {
    it("should update item quantity of item", () => {
      const basket: Basket = {
        totalCost: "$10.00",
        items: [
          {
            id: "1",
            name: "Product Name",
            quantity: 1,
            totalCost: "$10.00",
            unitCost: 1000,
          },
        ],
      };

      const result = updateItemWithNewQuantity(basket, "1", 4);

      expect(result).toEqual({
        totalCost: "$40.00",
        items: [
          {
            id: "1",
            name: "Product Name",
            quantity: 4,
            totalCost: "$40.00",
            unitCost: 1000,
          },
        ],
      });
    });
  });
});
