export function formatPrice(price: number): string {
  const costWithDecimal = price / 100;
  return new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
  }).format(costWithDecimal);
}
