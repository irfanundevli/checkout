import { Link } from "react-router-dom";
import { formatPrice } from "../utilities/price-formatter";
import "./ProductList.scss";
import { useQuery } from "react-query";
import { Product } from "./App";

type Props = {
  handleAddToBasket: (product: Product) => void;
};

export const ProductList = ({ handleAddToBasket }: Props) => {
  const { isLoading, data: productList } = useQuery<Product[]>(
    "productList",
    () => fetch("/products").then((res) => res.json())
  );

  if (isLoading) return <div>"Loading..."</div>;

  return (
    <div className="c-products">
      <h1 className="c-products__header">Products</h1>

      <div className="c-products__cards">
        {productList?.map((product) => {
          return (
            <div key={product.id} className="c-products__card">
              <h4>{product.name}</h4>
              <p>{formatPrice(product.price)}</p>
              <button
                onClick={() => handleAddToBasket(product)}
                className="c-products__card-add-btn"
              >
                Add to Basket
              </button>
            </div>
          );
        })}
      </div>

      <div className="c-products__view-basket">
        <Link to="/basket">View Basket</Link>
      </div>
    </div>
  );
};
