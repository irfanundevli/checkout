import { Link } from "react-router-dom";
import { Basket } from "./App";
import "./ShoppingBasket.scss";

export type ShoppingBasketProps = {
  basket: Basket;
  handleResetAllItemsToZero: () => void;
  handleRemoveItem: (itemId: string) => void;
  handleItemQuantityChange: (itemId: string, newQuantity: number) => void;
};

export const ShoppingBasket = ({
  basket,
  handleResetAllItemsToZero,
  handleRemoveItem,
  handleItemQuantityChange,
}: ShoppingBasketProps) => {
  return (
    <div className="c-shopping-basket">
      <h1>Shopping Basket</h1>
      {basket.items.map((item) => {
        return (
          <div key={item.id} className="c-shopping-basket__item">
            <p className="c-shopping-basket__item-name">{item.name}</p>
            <input
              className="c-shopping-basket__item-quantity"
              value={item.quantity}
              onChange={(e) => {
                handleItemQuantityChange(item.id, Number(e.target.value));
              }}
            ></input>
            <p className="c-shopping-basket__item-cost"> {item.totalCost}</p>
            <a
              href="/#"
              onClick={(e) => {
                e.preventDefault();
                handleRemoveItem(item.id);
              }}
            >
              X
            </a>
          </div>
        );
      })}
      {basket.items.length > 0 && (
        <div className="c-shopping-basket__summary">
          <div className="c-shopping-basket__total-cost">
            {basket.totalCost}
          </div>
          <div
            className="c-shopping-basket__clear-btn"
            onClick={() => handleResetAllItemsToZero()}
          >
            Clear
          </div>
          <div className="c-shopping-basket__checkout-btn">
            <Link
              to="/checkout"
              className="c-shopping-basket__checkout-btn-text"
            >
              {"Check out >"}
            </Link>
          </div>
        </div>
      )}
    </div>
  );
};
