import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { MemoryRouter } from "react-router";
import { Basket } from "./App";
import { ShoppingBasket, ShoppingBasketProps } from "./ShoppingBasket";

describe("shopping basket component", () => {
  const mockHandleResetAllItemsToZero = jest.fn();
  const mockHandleRemoveItem = jest.fn();
  const mockHandleItemQuantityChange = jest.fn();

  const basket: Basket = {
    totalCost: "Basket Total Cost",
    items: [
      {
        id: "1",
        name: "Product Name",
        quantity: 122,
        totalCost: "Item Total Cost",
        unitCost: 1000,
      },
    ],
  };

  const defaultProps: ShoppingBasketProps = {
    basket,
    handleResetAllItemsToZero: mockHandleResetAllItemsToZero,
    handleRemoveItem: mockHandleRemoveItem,
    handleItemQuantityChange: mockHandleItemQuantityChange,
  };

  const renderShoppingBasketWith = (props: ShoppingBasketProps) =>
    render(
      <MemoryRouter>
        <ShoppingBasket {...props} />
      </MemoryRouter>
    );

  it("should render component successfully", () => {
    renderShoppingBasketWith(defaultProps);

    expect(screen.getByText("Shopping Basket")).toBeInTheDocument();
  });

  it("should render item name", () => {
    renderShoppingBasketWith(defaultProps);

    expect(screen.getByText("Product Name")).toBeInTheDocument();
  });

  it("should render item quantity", () => {
    renderShoppingBasketWith(defaultProps);

    expect(screen.getByDisplayValue("122")).toBeInTheDocument();
  });

  it("should render item  total cost", () => {
    renderShoppingBasketWith(defaultProps);

    expect(screen.getByText("Item Total Cost")).toBeInTheDocument();
  });

  it("should render basket  total cost", () => {
    renderShoppingBasketWith(defaultProps);

    expect(screen.getByText("Basket Total Cost")).toBeInTheDocument();
  });

  it("should call handleRemoveItem callback func when X button is clicked", () => {
    renderShoppingBasketWith(defaultProps);
    userEvent.click(screen.getByText("X"));

    expect(mockHandleRemoveItem).toHaveBeenCalledTimes(1);
  });

  it("should call handleResetAllItemsToZero callback func when Clear button is clicked", () => {
    renderShoppingBasketWith(defaultProps);
    userEvent.click(screen.getByText("Clear"));

    expect(mockHandleResetAllItemsToZero).toHaveBeenCalledTimes(1);
  });

  it("should call handleItemQuantityChange callback func when quantity is changed", () => {
    renderShoppingBasketWith(defaultProps);
    userEvent.type(screen.getByDisplayValue("122"), "1");

    expect(mockHandleItemQuantityChange).toHaveBeenCalledTimes(1);
  });
});
