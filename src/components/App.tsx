import { BrowserRouter, Switch, Route } from "react-router-dom";
import { ShoppingBasket } from "./ShoppingBasket";
import { ProductList } from "./ProductList";
import {
  addProductToBasket,
  removeItemFromBasketByItemId,
  resetAllBasketItemsQuantityToZero,
  updateItemWithNewQuantity,
} from "../utilities/shopping-basket-operations";
import { useState } from "react";
import "./App.scss";
import { Checkout } from "./Checkout";
import { QueryClient, QueryClientProvider } from "react-query";

const queryClient = new QueryClient();

export type Product = {
  id: string;
  name: string;
  price: number;
};

export type BasketItem = Pick<Product, "id" | "name"> & {
  quantity: number;
  unitCost: number;
  totalCost: string;
};

export type Basket = {
  items: BasketItem[];
  totalCost: string;
};

function App() {
  const [basket, setBasket] = useState<Basket>({
    items: [],
    totalCost: "0",
  });

  const addToBasket = (product: Product) => {
    setBasket(addProductToBasket(product, basket));
  };

  const resetAllItemsToZero = () => {
    setBasket(resetAllBasketItemsQuantityToZero(basket));
  };

  const removeItem = (itemId: string) => {
    setBasket(removeItemFromBasketByItemId(basket, itemId));
  };

  const changeItemQuantity = (itemId: string, newQuantity: number) => {
    setBasket(updateItemWithNewQuantity(basket, itemId, newQuantity));
  };

  return (
    <div className="c-app__container">
      <QueryClientProvider client={queryClient}>
        <BrowserRouter>
          <Switch>
            <Route exact path="/">
              <ProductList handleAddToBasket={addToBasket} />
            </Route>
            <Route path="/basket">
              <ShoppingBasket
                basket={basket}
                handleResetAllItemsToZero={resetAllItemsToZero}
                handleRemoveItem={removeItem}
                handleItemQuantityChange={changeItemQuantity}
              />
            </Route>
            <Route path="/checkout">
              <Checkout />
            </Route>
          </Switch>
        </BrowserRouter>
      </QueryClientProvider>
    </div>
  );
}

export default App;
